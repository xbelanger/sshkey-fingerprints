# sshkey-fingerprints and sshkey-dnssec

The sshkey-fingerprints script will generate the MD5 and SHA256 fingerprints for each public key used by the local OpenSSH server. The result is to be published in order for the users to check the authenticity of the server at the first connection.

The sshkey-dnssec script will generate the SSHFP records to be added to the DNS zone, those records can be used by the VerifyHostKeyDNS option of the OpenSSH client.


