#!/bin/bash

# Xavier Belanger
# August  7, 2022

# this script is covered by the BSD 2-Clause License

# output the SSHFP fingerprints for each of
# the OpenSSH server public keys

keys=$(/usr/bin/find /etc/ssh -name 'ssh_host_*_key.pub')

if [ -z "$keys" ]
then
	/bin/printf "No public OpenSSH key\n"
	exit 1
else
	/bin/printf "\n"

	for key in $keys
	do
		/bin/printf "; $key\n"
		/usr/bin/ssh-keygen -r $(/bin/hostname) -f $key
		/bin/printf "\n"
	done
fi

# EoF
