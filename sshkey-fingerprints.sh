#!/bin/bash

# Xavier Belanger
# August  7, 2022

# this script is covered by the BSD 2-Clause License

# output the MD5 and SHA256 fingerprints for each of
# the OpenSSH server public keys

keys=$(/usr/bin/find /etc/ssh -name 'ssh_host_*_key.pub')

if [ -z "$keys" ]
then
	/bin/printf "No public OpenSSH key\n"
	exit 1
else
	/bin/printf "Hostname: $(/bin/hostname --fqdn)\n\n"

	for key in $keys
	do
		/bin/printf "$key\n"
		/usr/bin/ssh-keygen -l -E md5 -f $key | /bin/cut -d" " -f2 | /bin/sed -e 's/:/ /'
		/usr/bin/ssh-keygen -l -E sha256 -f $key | /bin/cut -d" " -f2 | /bin/sed -e 's/:/ /'
		/bin/printf "\n"
	done
fi

# EoF
